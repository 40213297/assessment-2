﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Adrianna Kaminska
//40213297
//Guest class where are parameters and methods for guests and extras
//09/12/2016

namespace _40213297CSD2
{
    [Serializable]
    class Guests
    {
        //private int
        private int age;
        private int bookingrefnumb;
        //private bools 
        bool guest1;
        bool guest2;
        bool guest3;
        bool guest4;
        bool breakfast;
        bool eveningmeal;
        bool carhire;
        bool underaged;
        bool underaged2;
        bool underaged3;
        bool underaged4;
        //private strings
        private string invoicepayment;
        private string guestsnames;
        private string guestsnames2;
        private string guestsnames3;
        private string guestsnames4;
        private string passport;
        private string passports2;
        private string passports3;
        private string passports4;
        private string bookingloadnumb;
        private string drivername;
        private string breakfast_req;
        private string eveningmeal_req;



        public Guests()
        {
            guest1 = false;
            guest2 = false;
            guest3 = false;
            guest4 = false;
            breakfast = false;
            eveningmeal = false;
            carhire = false;
            underaged = false;
            underaged2 = false;
            underaged3 = false;
            underaged4 = false;
        }
        public int BookingRef
        { get { return bookingrefnumb; } set { bookingrefnumb = value; } }
        public bool Guest1 { get { return guest1; } set { guest1 = value; } }
        public bool Guest2 { get { return guest2; } set { guest2 = value; } }
        public bool Guest3 { get { return guest3; } set { guest3 = value; } }
        public bool Guest4 { get { return guest4; } set { guest4 = value; } }

        public string BookingLoad
        {
            get { return bookingloadnumb; }
            set { bookingloadnumb = value; }
        }
        public string DriverName
        {
            get { return drivername; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    drivername = value;
                }
                else
                { drivername = value; }
            }
        }
        public bool Breakfast
        {
            get { return breakfast; }
            set
            {
                breakfast = value;
            }
        }
        public bool Eveningmeal
        {
            get { return eveningmeal; }
            set
            {
                eveningmeal = value;
            }
        }
        public bool CarHire
        {
            get { return carhire; }
            set
            {
                carhire = value;
            }
        }
        public string InvoicePayment
        {
            get { return invoicepayment; }
            set { invoicepayment = value; }
        }
        //bools presenting gueusts
        public bool Underaged
        { get { return underaged; } set { underaged = value; } }
        public bool Underaged2 { get { return underaged2; } set { underaged2 = value; } }
        public bool Underaged3 { get { return underaged3; } set { underaged3 = value; } }
        public bool Underaged4 { get { return underaged4; } set { underaged4 = value; } }
        //method Costs for invoice for customer per each guest
        public int Costs()
        {

            int value = 50;
            if (guest1)
            {
                return (int)(value + 50);
            }
            if (guest2)
            {
                return (int)(value + 100);
            }
            if (guest3)
            {
                return (int)(value + 150);
            }
            if (guest4)
            {
                return (int)(value + 200);
            }
            else
            {
                return value;
            }

        }
        //public int presenting cost of the breakfast
        public int ExtraCostBreakfast()
        {
            int value = 0;
            if (breakfast)
            {
                return (int)(value + 5);
            }
            else
            {
                return value;
            }
        }
        public string Breakfast_Req
        {
            get { return breakfast_req; }
            set
            {
                if (value == String.Empty || value == null)
                { breakfast_req = value; }
                else
                { breakfast_req = value; }
            }
        }
        public string EveningMeal_Req
        {
            get { return eveningmeal_req; }
            set
            {
                if (value == String.Empty || value == null)
                { eveningmeal_req = value; }
                else
                { eveningmeal_req = value; }
            }
        }
        //public int presenting cost of the evening meal
        public int ExtraCostEveningMeal()
        {
            int value = 0;
            if (eveningmeal)
            {
                return (int)(value + 15);
            }
            else { return value; }
        }
        //public int presenting cost of car hire
        public int ExtraCarHire()
        {
            int value = 0;
            if (carhire)
            {
                return (int)(value + 50);
            }
            else { return value; }
        }
        // public int which sums all the costs
        public int CostsSums()
        {
            int cost = Costs();
            int eveningmeal = ExtraCostEveningMeal();
            int breakfast = ExtraCostBreakfast();
            int carhire = ExtraCarHire();
            int full = (cost + eveningmeal + breakfast + carhire);
            return full;
        }
        //Age discount method which through if statements is checking the amount of discount if the checkbox 0-18 is checked
        public int AgeDiscount()
        {
            int value = 0;
            //if one guest is underaged
            if (underaged && underaged2 && underaged3 && underaged4)
            {
                return (int)(value - 80);
            }
            if (underaged && underaged2 && underaged3)
            {
                return (int)(value - 60);
            }
            if (underaged && underaged2)
            {
                return (int)(value - 40);
            }
            if (underaged && underaged3)
            {
                return (int)(value - 40);
            }
            if (underaged && underaged4)
            {
                return (int)(value - 40);
            }
            if (underaged2 && underaged3)
            {
                return (int)(value - 40);
            }
            if (underaged2 && underaged4)
            {
                return (int)(value - 40);
            }
            if (underaged3 && underaged4)
            {
                return (int)(value - 40);
            }
            if (underaged)
            {
                return (int)(value - 20);
            }
            if (underaged2)
            {
                return (int)(value - 20);
            }
            if (underaged3)
            {
                return (int)(value - 20);
            }

            if (underaged4)
            {
                return (int)(value - 20);
            }
            else { return value; }
        }
        //this method counts full price for the invoice 
        public int CostWithDiscount()
        {
            int cost = Costs();
            int eveningmeal = ExtraCostEveningMeal();
            int breakfast = ExtraCostBreakfast();
            int agediscount = AgeDiscount();
            int carhire = ExtraCarHire();
            int full = (cost + eveningmeal + breakfast + carhire);
            int discfull = (full + agediscount);
            return discfull;
        }
        //it checks if the value inserted into age text box is correct. If not it sends error messages
        public int Age
        {
            get { return age; }
            set
            {
                if (value > 101)
                {
                    throw new ArgumentException("Please, type correct age");
                }
                if (value < 0)
                {
                    throw new ArgumentException("Please, type correct age");
                }
                else
                {
                    age = value;
                }
            }
        }
        public string GuestName
        {
            get { return guestsnames; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guestsnames = value;
                }
                else
                { guestsnames = value; }
            }
        }
        public string GuestName2
        {
            get { return guestsnames2; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guestsnames2 = value;
                }
                else
                { guestsnames2 = value; }
            }
        }
        public string GuestName3
        {
            get { return guestsnames3; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guestsnames3 = value;
                }
                else
                { guestsnames3 = value; }
            }
        }
        public string GuestName4
        {
            get { return guestsnames4; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guestsnames4 = value;
                }
                else
                { guestsnames = value; }
            }
        }
        public string Passport
        {
            get
            {
                return passport;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    passport = value;
                }
                else
                { passport = value; }
            }
        }
        public string Passports2
        {
            get
            {
                return passports2;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    passport = value;
                }
                else
                { passport = value; }
            }
        }
        public string Passports3
        {
            get
            {
                return passports3;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    passport = value;
                }
                else
                { passport = value; }
            }
        }
        public string Passports4
        {
            get
            {
                return passports4;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    passport = value;
                }
                else
                { passport = value; }
            }
        }
      
    }
}
