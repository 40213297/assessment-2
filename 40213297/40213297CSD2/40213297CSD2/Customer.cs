﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
//Adrianna Kaminska
//40213297
//Customer class and logic for the parts connected with the customer 
//09/12/2016
namespace _40213297CSD2
{
    [Serializable]
    class Customer
    {
        private string custrefload;
        //private values, used later in public set and get methods
        private string name;
        private string address;
        private int custnumb;
        private DateTime arrivaldate;
        private DateTime deportdate;
        
        //public strings with methods
        public string CustRefLoad
        {
            get { return custrefload; }
            set { custrefload = value; }
        }
        public int CustNumb
        {
            get { return custnumb; }
            set { custnumb = value; }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Type your full name");
                }
                else
                {
                    name = value;
                }
            }
        }
        public string Address
        {
            get { return address; }
            set
            {
                if (value == String.Empty || value == null)
                {
                    address = value;
                }
                else
                {
                    address = value;
                }
                
            }
        }
        //public date time for date picks for arrival and deport
        public DateTime ArrivalDate
        {
            get { return arrivaldate; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please, choose the date of your arrival");
                }
                else
                {
                    arrivaldate = value;
                }
            }
        }
        public DateTime DeportDate
        {
            get { return deportdate; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please, choose the date when you are going to leave us");
                }
                else
                {
                    deportdate = value;
                }
            }
        }





    }
}
