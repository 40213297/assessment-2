﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//Adrianna Kaminska
//Logic for Invoice 
//40213297
//09/12/16

namespace _40213297CSD2
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        //private fields
        private DateTime deportdate;
        private DateTime arrivaldate;
        private string name;
        private string address;
        private bool guest1 = false;
        private bool guest2 = false;
        private bool guest3 = false;
        private bool guest4 = false;
        private bool eveningmeal = false;
        private bool carhire = false;
        private bool breakfast = false;
        private bool underaged = false;
        private int age;
        private int cost;
        private int eveningmealcost;
        private int discount;
        private int breakfastcost;
        private int costwithdiscount;
        private int fullcosts;
        private string am;
        private int carhirecost;
        private string drivername;
        

        Customer customerInstance = new Customer();
 
        public Invoice()
        {
            InitializeComponent();
        }

        private void InvoiceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }
        //close button
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        public void SetProperties(string Name, string Address, int Cost, bool EveningMeal, bool CarHire, bool Breakfast, bool Underaged, bool Guest1, bool Guest2, bool Guest3, bool Guest4, string GuestNames, int ExtraCosts, int CostsSums, int AgeDiscount, int BreakfastCost, int CostWithDiscount, bool Underaged2, bool Underaged3, bool Underaged4, DateTime DeportDate, DateTime ArrivalDate, string Amount, int ExtraCarHire, string DriverName)
        {
            this.name = Name;
            this.address = Address;
            this.cost = Cost;
            this.guest1 = Guest1;
            this.guest2 = Guest2;
            this.guest3 = Guest3;
            this.guest4 = Guest4;
            this.underaged = Underaged;
            this.eveningmeal = EveningMeal;
            this.breakfast = Breakfast;
            this.carhire = CarHire;
            this.carhirecost = ExtraCarHire;
            this.eveningmealcost = ExtraCosts;
            this.fullcosts = CostsSums;
            this.discount = AgeDiscount;
            this.breakfastcost = BreakfastCost;
            this.costwithdiscount = CostWithDiscount;
            this.deportdate = DeportDate;
            this.arrivaldate = ArrivalDate;
            this.am = Amount;
            this.drivername = DriverName;
        
        }
       
        public void DisplayInvoice()
        {  
                InvoiceTextBox.Text = "Invoice for " + name + " " + cost + " for guests and for evening meals " + eveningmealcost + " , for breakfasts " + breakfastcost + " ,for car hire: " + carhirecost + " The costs " + fullcosts + " After age discount " + costwithdiscount  + "  " + " The full amount for guests and amount of days: " + Sum() + " " +"From when to when you planning to visit us: " + arrivaldate + " till " + deportdate + "         Total amount of days:" + am + "                 " + "   Your driver name: " + drivername;
        }
        //it calculated the full amount
        public int Sum()
        {
            int full = fullcosts;
            int afterdiscount = costwithdiscount;
            int days = int.Parse(am);
            int anothercosts = (costwithdiscount * days);
            return anothercosts;
        }
    }
}
