﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//Adrianna Kaminska
//40213297
//booking window class which holds all logic for bookings and customer
//09/12/16
namespace _40213297CSD2
{
    /// <summary>
    /// Interaction logic for Booking.xaml
    /// </summary>
    public partial class Booking : Window
    {
        Customer customerInstance = new Customer();
        Guests guestInstance = new Guests();
        Invoice invoiceInstance = new Invoice();
        public Booking()
        {
            InitializeComponent();
            //hidden checkboxes and textboxes
            Name1TextBox.Visibility = Visibility.Hidden;
            Passport1TextBox.Visibility = Visibility.Hidden;
            Name2TextBox.Visibility = Visibility.Hidden;
            Passport2TextBox.Visibility = Visibility.Hidden;
            Name3TextBox.Visibility = Visibility.Hidden;
            Passport3TextBox.Visibility = Visibility.Hidden;
            Name4TextBox.Visibility = Visibility.Hidden;
            Passport4TextBox.Visibility = Visibility.Hidden;
            age1.Visibility = Visibility.Hidden;
            age2.Visibility = Visibility.Hidden;
            age3.Visibility = Visibility.Hidden;
            age4.Visibility = Visibility.Hidden;
            guest1kid.Visibility = Visibility.Hidden;
            guest2kid.Visibility = Visibility.Hidden;
            guest3kid.Visibility = Visibility.Hidden;
            guest4kid.Visibility = Visibility.Hidden;
            eveningmealtextbox.Visibility = Visibility.Hidden;
            breakfasttextbox.Visibility = Visibility.Hidden;

         
            
        }
 //button generating number for booking ref
        private void BookinfRefGenerateButton_Click(object sender, RoutedEventArgs e)
        {
            Random bookingrnd = new Random();
            int bookingrndvalue = bookingrnd.Next(999, 2000);
            BookRefNumbTextBox.Text = bookingrndvalue.ToString();

        }
        //uncheck private checkboxes events checking how many customers wants user, and basing on this how many of the text and checkboxes will be seeing 
        private void guest0_Checked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                guest1.IsChecked = false;
                guest2.IsChecked = false;
                guest3.IsChecked = false;
                guest4.IsChecked = false;
            }
        }

        private void guest1_Checked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name1TextBox.Visibility = Visibility.Visible;
                Passport1TextBox.Visibility = Visibility.Visible;
                age1.Visibility = Visibility.Visible;
                guest1kid.Visibility = Visibility.Visible;
              
                    guest2.IsChecked = false;
                    guest0.IsChecked = false;
                    guest3.IsChecked = false;
                    guest4.IsChecked = false;
                
            }
        }

        private void guest2_Checked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name2TextBox.Visibility = Visibility.Visible;
                Passport2TextBox.Visibility = Visibility.Visible;
                Name1TextBox.Visibility = Visibility.Visible;
                Passport1TextBox.Visibility = Visibility.Visible;
                age1.Visibility = Visibility.Visible;
                age2.Visibility = Visibility.Visible;
                guest1kid.Visibility = Visibility.Visible;
                guest2kid.Visibility = Visibility.Visible;

                guest0.IsChecked = false;
                guest1.IsChecked = false;
                guest3.IsChecked = false;
                guest4.IsChecked = false;
            }
        }

        private void guest3_Checked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name2TextBox.Visibility = Visibility.Visible;
                Passport2TextBox.Visibility = Visibility.Visible;
                Name1TextBox.Visibility = Visibility.Visible;
                Passport1TextBox.Visibility = Visibility.Visible;
                Name3TextBox.Visibility = Visibility.Visible;
                Passport3TextBox.Visibility = Visibility.Visible;
                age3.Visibility = Visibility.Visible;
                age1.Visibility = Visibility.Visible;
                age2.Visibility = Visibility.Visible;
                guest1kid.Visibility = Visibility.Visible;
                guest2kid.Visibility = Visibility.Visible;
                guest3kid.Visibility = Visibility.Visible;

                guest0.IsChecked = false;
                guest1.IsChecked = false;
                guest2.IsChecked = false;
                guest4.IsChecked = false;
            }
        }

        private void guest4_Checked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name2TextBox.Visibility = Visibility.Visible;
                Passport2TextBox.Visibility = Visibility.Visible;
                Name1TextBox.Visibility = Visibility.Visible;
                Passport1TextBox.Visibility = Visibility.Visible;
                Name3TextBox.Visibility = Visibility.Visible;
                Passport3TextBox.Visibility = Visibility.Visible;
                Name4TextBox.Visibility = Visibility.Visible;
                Passport4TextBox.Visibility = Visibility.Visible;
                age3.Visibility = Visibility.Visible;
                age1.Visibility = Visibility.Visible;
                age2.Visibility = Visibility.Visible;
                age4.Visibility = Visibility.Visible;
                guest1kid.Visibility = Visibility.Visible;
                guest2kid.Visibility = Visibility.Visible;
                guest3kid.Visibility = Visibility.Visible;
                guest4kid.Visibility = Visibility.Visible;

                guest1.IsChecked = false;
                guest0.IsChecked = false;
                guest2.IsChecked = false;
                guest3.IsChecked = false;
            }
        }

        private void guest4_unChecked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name1TextBox.Visibility = Visibility.Hidden;
                Passport1TextBox.Visibility = Visibility.Hidden;
                Name2TextBox.Visibility = Visibility.Hidden;
                Passport2TextBox.Visibility = Visibility.Hidden;
                Name3TextBox.Visibility = Visibility.Hidden;
                Passport3TextBox.Visibility = Visibility.Hidden;
                Name4TextBox.Visibility = Visibility.Hidden;
                Passport4TextBox.Visibility = Visibility.Hidden;
                age3.Visibility = Visibility.Hidden;
                age1.Visibility = Visibility.Hidden;
                age2.Visibility = Visibility.Hidden;
                age4.Visibility = Visibility.Hidden;
            }
        }
        private void guest3_unChecked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name1TextBox.Visibility = Visibility.Hidden;
                Passport1TextBox.Visibility = Visibility.Hidden;
                Name2TextBox.Visibility = Visibility.Hidden;
                Passport2TextBox.Visibility = Visibility.Hidden;
                Name3TextBox.Visibility = Visibility.Hidden;
                Passport3TextBox.Visibility = Visibility.Hidden;
                Name4TextBox.Visibility = Visibility.Hidden;
                Passport4TextBox.Visibility = Visibility.Hidden;
                age3.Visibility = Visibility.Hidden;
                age1.Visibility = Visibility.Hidden;
                age2.Visibility = Visibility.Hidden;
                age4.Visibility = Visibility.Hidden;
            }
        }
        private void guest2_unChecked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name1TextBox.Visibility = Visibility.Hidden;
                Passport1TextBox.Visibility = Visibility.Hidden;
                Name2TextBox.Visibility = Visibility.Hidden;
                Passport2TextBox.Visibility = Visibility.Hidden;
                Name3TextBox.Visibility = Visibility.Hidden;
                Passport3TextBox.Visibility = Visibility.Hidden;
                Name4TextBox.Visibility = Visibility.Hidden;
                Passport4TextBox.Visibility = Visibility.Hidden;
                age3.Visibility = Visibility.Hidden;
                age1.Visibility = Visibility.Hidden;
                age2.Visibility = Visibility.Hidden;
                age4.Visibility = Visibility.Hidden;
                guest1kid.Visibility = Visibility.Hidden;
                guest2kid.Visibility = Visibility.Hidden;
                guest3kid.Visibility = Visibility.Hidden;
                guest4kid.Visibility = Visibility.Hidden;
            }
        }
        private void guest1_unChecked(object sender, RoutedEventArgs e)
        {
            if(true)
            {
                Name1TextBox.Visibility = Visibility.Hidden;
                Passport1TextBox.Visibility = Visibility.Hidden;
                Name2TextBox.Visibility = Visibility.Hidden;
                Passport2TextBox.Visibility = Visibility.Hidden;
                Name3TextBox.Visibility = Visibility.Hidden;
                Passport3TextBox.Visibility = Visibility.Hidden;
                Name4TextBox.Visibility = Visibility.Hidden;
                Passport4TextBox.Visibility = Visibility.Hidden;
                age3.Visibility = Visibility.Hidden;
                age1.Visibility = Visibility.Hidden;
                age2.Visibility = Visibility.Hidden;
                age4.Visibility = Visibility.Hidden;
                guest1kid.Visibility = Visibility.Hidden;
                guest2kid.Visibility = Visibility.Hidden;
                guest3kid.Visibility = Visibility.Hidden;
                guest4kid.Visibility = Visibility.Hidden;
            }
        }

        private void age1_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void age2_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void age3_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void age4_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void InvoiceButton_Click(object sender, RoutedEventArgs e)
        {         //logic for the Invoice button
            Invoice windowInvoice = new Invoice();
            windowInvoice.Show();
            //customer and driver, names and address text boxes
            customerInstance.Name = NewCustNameTextBox.Text;
            //guests names

            guestInstance.GuestName = Name1TextBox.Text ;
            guestInstance.GuestName2 = Name2TextBox.Text;
            guestInstance.GuestName3 = Name3TextBox.Text ;
            guestInstance.GuestName4 = Name4TextBox.Text;
            //guest checkboxes
            guestInstance.Guest1 = (bool)guest1.IsChecked;
            guestInstance.Guest2 = (bool)guest2.IsChecked;
            guestInstance.Guest3 = (bool)guest3.IsChecked;
            guestInstance.Guest4 = (bool)guest4.IsChecked;
            //extras checkboxes
            guestInstance.Breakfast = (bool)breakfastcheckbox.IsChecked;
            guestInstance.Eveningmeal = (bool)eveningmealcheckbox.IsChecked;
            guestInstance.CarHire = (bool)CarHireCheckBox.IsChecked;
            //age discounts checkboxes
            guestInstance.Underaged = (bool)guest1kid.IsChecked;
            guestInstance.Underaged2 = (bool)guest2kid.IsChecked;
            guestInstance.Underaged3 = (bool)guest3kid.IsChecked;
            guestInstance.Underaged4 = (bool)guest4kid.IsChecked;
            //guests ages
            age1.Text = guestInstance.Age.ToString();
            age2.Text = guestInstance.Age.ToString();
            age3.Text = guestInstance.Age.ToString();
            age4.Text = guestInstance.Age.ToString(); 
            //dates
            customerInstance.DeportDate = Departuredate.SelectedDate.Value.Date;
            customerInstance.ArrivalDate = Arrivaldate.SelectedDate.Value.Date;
            //code for datepickers
            if (!Arrivaldate.SelectedDate.HasValue || !Departuredate.SelectedDate.HasValue)
            {
                MessageBox.Show("Select dates when you plan to arrive and leave us");
                return;
            }
            DateTime start = Arrivaldate.SelectedDate.Value.Date;
            DateTime finish = Departuredate.SelectedDate.Value.Date;
            TimeSpan difference = finish.Subtract(start);
            string amount = difference.TotalDays.ToString();

            //setting properties for the invoice window and calling  displayInvoice method 
            windowInvoice.SetProperties(customerInstance.Name, customerInstance.Address, guestInstance.Costs(), guestInstance.Eveningmeal, guestInstance.CarHire, guestInstance.Breakfast, guestInstance.Underaged, guestInstance.Guest1, guestInstance.Guest2, guestInstance.Guest3, guestInstance.Guest4, guestInstance.GuestName, guestInstance.ExtraCostEveningMeal(), guestInstance.CostsSums(), guestInstance.AgeDiscount(), guestInstance.ExtraCostBreakfast(), guestInstance.CostWithDiscount(), guestInstance.Underaged2, guestInstance.Underaged3,guestInstance.Underaged4, customerInstance.DeportDate, customerInstance.ArrivalDate, amount, guestInstance.ExtraCarHire(), guestInstance.DriverName);
            windowInvoice.DisplayInvoice();
        }
        //closing button
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        //button open registration window which presents all the gather information about customer and booking
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            //logic for submit button
            customerInstance.Name = NewCustNameTextBox.Text;
            customerInstance.Address = NewCustAddressTextBox.Text;
            //dietary requirements
            guestInstance.Breakfast_Req = breakfasttextbox.Text;
            guestInstance.EveningMeal_Req = eveningmealtextbox.Text;
            //guests names text boxes

            guestInstance.GuestName = Name1TextBox.Text;
            guestInstance.GuestName2 = Name2TextBox.Text;
            guestInstance.GuestName3 = Name3TextBox.Text;
            guestInstance.GuestName4 = Name4TextBox.Text;
            //guest checkboxes
            guestInstance.Guest1 = (bool)guest1.IsChecked;
            guestInstance.Guest2 = (bool)guest2.IsChecked;
            guestInstance.Guest3 = (bool)guest3.IsChecked;
            guestInstance.Guest4 = (bool)guest4.IsChecked;
            //guests passports textboxes

            guestInstance.Passport = Passport1TextBox.Text;
            guestInstance.Passports2 = Passport2TextBox.Text;
            guestInstance.Passports3 = Passport3TextBox.Text;
            guestInstance.Passports4 = Passport4TextBox.Text ;
            //driver name
           
            guestInstance.DriverName = DriverNameTextBox.Text;
            //customer number and booking number
            guestInstance.BookingRef = int.Parse(BookRefNumbTextBox.Text);
            customerInstance.CustNumb = int.Parse(CustomerRefNumGenerateTextBox.Text);
            //checkboxes for extras
            guestInstance.Breakfast = (bool)breakfastcheckbox.IsChecked;
            guestInstance.Eveningmeal = (bool)eveningmealcheckbox.IsChecked;
            try
            {
                
                customerInstance.Name = NewCustNameTextBox.Text;
                
                //error - if u will not show your guests appears error
                guestInstance.Age = int.Parse(age1.Text);
                guestInstance.Age = int.Parse(age2.Text);
                guestInstance.Age = int.Parse(age3.Text);
                guestInstance.Age = int.Parse(age4.Text);
                customerInstance.DeportDate = Departuredate.SelectedDate.Value.Date;
                customerInstance.ArrivalDate = Arrivaldate.SelectedDate.Value.Date;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            
            


            if (!Arrivaldate.SelectedDate.HasValue || !Departuredate.SelectedDate.HasValue)
            {
                MessageBox.Show("Select dates when you plan to arrive and leave us");
                return;
            }
            DateTime start = Arrivaldate.SelectedDate.Value.Date;
            DateTime finish = Departuredate.SelectedDate.Value.Date;
            TimeSpan difference = finish.Subtract(start);
            string amount = difference.TotalDays.ToString();
            
            
            Registration submitreg = new Registration();
            submitreg.Show();


            submitreg.SetProperties(customerInstance.Name, customerInstance.Address, guestInstance.Costs(), guestInstance.Eveningmeal, guestInstance.Breakfast, guestInstance.Underaged, guestInstance.Guest1, guestInstance.Guest2, guestInstance.Guest3, guestInstance.Guest4, guestInstance.GuestName, guestInstance.ExtraCostEveningMeal(), guestInstance.CostsSums(), customerInstance.CustNumb, guestInstance.BookingRef, guestInstance.GuestName2, guestInstance.GuestName3, guestInstance.GuestName4, guestInstance.Passport, guestInstance.Passports2, guestInstance.Passports3, guestInstance.Passports4, guestInstance.DriverName, guestInstance.Breakfast_Req, guestInstance.EveningMeal_Req, amount, guestInstance.GuestName);
            submitreg.DisplaySubmit();
        }

        //Generates random number between 999 and 2000, which represents customer reference number
        private void CustomerRefNumbButton_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            int rndvalue = rnd.Next(999, 2000);
            CustomerRefNumGenerateTextBox.Text = rndvalue.ToString();
        }

        private void eveningmealcheckbox_Checked(object sender, RoutedEventArgs e)
        {
            eveningmealtextbox.Visibility = Visibility.Visible;
        }
        private void eveningmealcheckbox_unChecked(object sender, RoutedEventArgs e)
        {
            eveningmealtextbox.Visibility = Visibility.Hidden;
        }

        private void breakfastcheckbox_Checked(object sender, RoutedEventArgs e)
        {
            breakfasttextbox.Visibility = Visibility.Visible;
        }
        private void breakfastcheckbox_unChecked(object sender, RoutedEventArgs e)
        {
            breakfasttextbox.Visibility = Visibility.Hidden;
        }

        private void guest1kid_Checked(object sender, RoutedEventArgs e)
        {   
        }

        private void guest2kid_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void guest3kid_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void guest4kid_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CarHireCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Name1TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
