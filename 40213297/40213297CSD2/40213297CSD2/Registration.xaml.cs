﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Text;
using System.Linq;
using Microsoft.Win32;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

//Adrianna Kaminska
//40213297
//code for the registration window
//09/12/16
namespace _40213297CSD2
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    [Serializable]
    public partial class Registration : Window
    {
        //privates used later as a part of the parameters from Customer
        private string address;
        private int age; 
        //arrival date should be here;
        private int customerRegNumb;
        private int cost;
        private bool guest2;
        private bool guest1;
        private bool guest3;
        private bool guest4;
        private bool underaged;
        private bool eveningmeal;
        private bool breakfast;
        private bool carhire;
        private int extracosts;
        private int fullcosts;
        private int customerrefnumb;
        private int bookingref;
        private int sum;
        private string name;
        private string guestname;
        private string guestname2;
        private string guestname3;
        private string guestName4;
        private string guestname1;
        private string passport;
        private string passport2;
        private string passport3;
        private string passport4;
        private string drivername;
        private string eveningmeal_req;
        private string breakfast_req;
        private string amountsum;
        

        Guests guestInstance = new Guests();
        Customer customerInstance = new Customer();
        Invoice invoiceInstance = new Invoice();
 
       
        public Registration()
        {
            InitializeComponent();
        }

        //logic for the save button 
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string fileName = bookingref.ToString();
            string directoryName = customerrefnumb.ToString();
            string target = @"C:\Users\Adrianna\OneDrive\Documents\" + directoryName;
            if (!Directory.Exists(target))
            {
                Directory.CreateDirectory(target);
            }
            Environment.CurrentDirectory = (target);
                BinaryFormatter formatter = new BinaryFormatter();
                using (Stream streamst = File.Open(fileName, FileMode.CreateNew)) 
                   using(StreamWriter writer = new StreamWriter(streamst))
                {
                    writer.Write(TextBoxFiles.Text);
                formatter.Serialize(streamst, fileName);
                }
                MessageBox.Show("Your Customer Account been created!");
        }

          
        //logic for the loading button
        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {             
          
            LoadCustNumbTextBox.Text = customerInstance.CustRefLoad;
            BookingRefLoadTextBox.Text = guestInstance.BookingLoad;
            string LoadfileName = BookingRefLoadTextBox.ToString();
            string LoaddirectoryName = LoadCustNumbTextBox.ToString();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            BinaryFormatter formatter = new BinaryFormatter();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFileDialog.FileName = LoadfileName;
            if (openFileDialog.ShowDialog() == true)
            {
                TextBoxFiles.Text = File.ReadAllText(openFileDialog.FileName);
            }
        }
        //closing button
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBoxFiles_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }
         

        //display method
        public void SetProperties(string Name, string Address, int Cost, bool EveningMeal, bool Breakfast, bool Underaged, bool Guest1, bool Guest2, bool Guest3, bool Guest4, string GuestNames, int ExtraCosts, int CostsSums, int CustNumb, int BookingRef, string GuestName2, string GuestName3, string GuestName4, string Passport, string Passport2, string Passport3, string Passport4, string DriverName, string Breakfast_Requirements, string EveningMeal_Requirements,string Amount, string GuestName)
        {
            this.name = Name;
            this.address = Address;
            this.cost = Cost;
            this.guest1 = Guest1;
            this.guest2 = Guest2;
            this.guest3 = Guest3;
            this.guest4 = Guest4;
            this.underaged = Underaged;
            this.eveningmeal = EveningMeal;
            this.breakfast = Breakfast;
            this.extracosts = ExtraCosts;
            this.fullcosts = CostsSums;
            this.customerrefnumb = CustNumb;
            this.bookingref = BookingRef;
            this.guestname2 = GuestName2;
            this.guestname3 = GuestName3;
            this.guestName4 = GuestName4;
            this.guestname1 = GuestNames;
            this.passport = Passport;
            this.passport2 = Passport2;
            this.passport3 = Passport3;
            this.passport4 = Passport4;
            this.drivername = DriverName;
            this.breakfast_req = Breakfast_Requirements;
            this.eveningmeal_req = EveningMeal_Requirements;
            this.amountsum = Amount;
            this.guestname = GuestName;
        }
        public void DisplaySubmit()
        {
            TextBoxFiles.Text = "Customer Name: " + name + " Cutomer Address: " + address + " Customer Reference Number: " + customerrefnumb + " Customer Booking Reference Number: " + bookingref + " Guests: " + guestname + " , "+ passport + " , " + guestname2 + " , " + passport2 + " , " + guestname3 + " , " + passport3 + " , " + guestName4 + " , " + passport4 + " " + " Driver Name: " + drivername + "            " + " Amount of days " + amountsum + "                         " + "   Dietary Requirements - " + " for breakfast: " + breakfast_req + "       for evening meals: " + eveningmeal_req;
        }

    }
}
