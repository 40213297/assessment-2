﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//Adrianna Kaminska
//40213297
//main window class 
//09/12/16

namespace _40213297CSD2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Customer customerInstance = new Customer();
        public MainWindow()
        {
            InitializeComponent();
        }
        //opens registration window
        private void LogInButton_Click(object sender, RoutedEventArgs e)
        {
            Registration regwindow = new Registration();
            regwindow.Show();
        }
        //opens booking
        private void NewCustomerButton_Click(object sender, RoutedEventArgs e)
        {
           
            Booking windowBooking = new Booking();
            windowBooking.Show();
        }

        private void CustomerRefNumbButton_Click(object sender, RoutedEventArgs e)
        {
            
        }
        //closes 
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
